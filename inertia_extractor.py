import os
import io
import time
import pymesh
import numpy as np
from stl import mesh

def main():

    modelName = raw_input("Enter the stl model name: ")
    modelName = modelName + ".obj"

    #Import obj file and save as stl
    loaded_pymesh = pymesh.meshio.load_mesh(modelName, drop_zero_dim=False)
    stlName = modelName.replace(".obj", ".stl", 1)
    pymesh.meshio.save_mesh(stlName, loaded_pymesh)

    #Reimport stl file and extract mass data:
    if os.path.exists(stlName):
        loaded_stl = mesh.Mesh.from_file(stlName)
        #Line below returns a tuple
        volume, cog, inertia = loaded_stl.get_mass_properties()
        print("Volume                = {0}".format(volume))
        print("Position of COG       = {0}".format(cog))
        print("Inertia matrix at COG = {0}".format(inertia[0,:]))
        print("                        {0}".format(inertia[1,:]))
        print("                        {0}".format(inertia[2,:]))
        os.remove(stlName)
    else:
        print("Waiting for the file to finish saving")

if __name__ == "__main__":
    main()
